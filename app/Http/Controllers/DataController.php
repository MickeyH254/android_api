<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{

    public function select_table($sql_statement){
        $db_table = DB::select(DB::raw($sql_statement));
        $selected_table = json_encode($db_table);
        return $selected_table;
    }
    
}
